use chrono::*;

#[derive(Serialize)]
pub struct Metadata {
    pub title: String,
    pub date: String,
}

#[derive(Serialize)]
pub struct Article {
    pub metadata: Metadata,
    pub content: String,
    pub intro: String,
}

#[derive(Serialize)]
pub struct Blog {
    pub title: String,
    pub articles: Vec<Article>,
}

pub fn load_articles(path: &str) -> Vec<Article> {
    use std::fs;
    use std::io::Read;
    use std::process::Command;

    println!("Loading articles..");

    let mut articles = Vec::<Article>::new();

    let paths = fs::read_dir(path).unwrap();
    for path in paths {
        let path = path.unwrap().path();
        println!("\t{}", path.display());
        let mut file = fs::File::open(&path).unwrap();
        let mut file_content = String::new();
        file.read_to_string(&mut file_content).unwrap();

        let pandoc = Command::new("pandoc").args(&[path]).output().unwrap();
        let mut content = String::from_utf8(pandoc.stdout.clone()).unwrap();
        let metadata = extract_metadata(&mut content);

        let intro = if let Some(pos) = content.find("<h3") {
            content[0..pos].to_owned()
        } else {
            content.clone()
        };

        articles.push(Article {
            metadata,
            content,
            intro,
        });
    }

    articles.sort_by(|a, b| {
        let a_date = a.metadata.date.parse::<DateTime<Utc>>().unwrap();
        let b_date = b.metadata.date.parse::<DateTime<Utc>>().unwrap();
        b_date.cmp(&a_date)
    });

    return articles;
}

fn extract_metadata(content: &mut String) -> Metadata {
    use regex::Regex;

    let rgx_title = Regex::new(r"(?m)^Title: (.+)$").unwrap();
    let mut title = rgx_title.captures(content).unwrap()[1].to_owned();
    title = String::from(title.trim());
    let rgx_date = Regex::new(r"(?m)^Date: (.+)$").unwrap();
    let mut date = rgx_date.captures(content).unwrap()[1].to_owned();
    date = String::from(date.trim());

    let position = content.find("-->").unwrap();
    let new_content = &content[position + 3..content.len()].to_owned();
    content.clear();
    content.push_str(new_content);
    content.trim();

    Metadata { title, date }
}
