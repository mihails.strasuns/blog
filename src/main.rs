extern crate chrono;
extern crate handlebars;
extern crate regex;
#[macro_use]
extern crate serde_derive;
extern crate urlencoding;

mod articles;

use articles::*;

use handlebars::*;
use std::fs;
use std::io::Write;
use std::path::{Path, PathBuf};

fn main() {
    let articles = load_articles("articles/");

    println!("Registering templates..");

    let mut handlebars = Handlebars::new();
    handlebars.register_helper("url", Box::new(url_helper));
    handlebars
        .register_template_file("index", "templates/index.hbs")
        .unwrap();
    handlebars
        .register_template_file("article", "templates/article.hbs")
        .unwrap();

    println!("Rendering index page ..");

    fs::create_dir_all("generated/articles/").unwrap();

    let data = Blog {
        title: "Article List".to_owned(),
        articles: articles,
    };
    let html = handlebars.render("index", &data).unwrap();

    let mut file = fs::File::create("generated/index.html").unwrap();
    file.write_all(html.as_bytes()).unwrap();

    println!("Rendering article pages ..");

    for article in data.articles {
        println!("\t - {}", article.metadata.title);
        let html = handlebars.render("article", &article).unwrap();
        let path = title_to_path(&article.metadata.title);
        let mut file = fs::File::create(path).unwrap();
        file.write_all(html.as_bytes()).unwrap();
    }

    println!("Compiling CSS..");

    std::fs::copy(
        Path::new("templates").join("all.css"),
        Path::new("generated").join("all.css"),
    ).unwrap();

    println!("Done.");
}

fn url_helper(h: &Helper, _: &Handlebars, rc: &mut RenderContext) -> Result<(), RenderError> {
    let param = try!(
        h.param(0)
            .ok_or(RenderError::new("Param 0 is required for URL helper.",))
    );
    let title = param.value().render();
    let url = title_to_url(title);
    try!(rc.writer.write(url.into_bytes().as_ref()));
    Ok(())
}

fn title_to_url(title: String) -> String {
    let title = title.replace(" ", "_");
    let title = urlencoding::encode(&title);
    let title = format!("articles/{}.html", title);
    title
}

fn title_to_path(title: &str) -> PathBuf {
    let mut path = PathBuf::new();
    path.push("generated");
    path.push("articles");
    path.push(title.replace(" ", "_"));
    path.set_extension("html");
    path
}
