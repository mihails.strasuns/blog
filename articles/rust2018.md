<!--
Title: Most desired from Rust in 2018
Date: 2018-01-11T00:00:00Z
-->

Recently there was a [call for community blog
posts](https://blog.rust-lang.org/2018/01/03/new-years-rust-a-call-for-community-blogposts.html)
themed about ideas and expectation for Rust in the coming year. I definitely
can't count myself as part of community yet as my experience with Rust so far
has been purely about experimenting and research but it feels like a great
opportunity to share my thoughts so far.

Short summary: more love for `[no_std]` + shifting ergonomics initiative towards
libraries and idioms.

### Uniformly applicable libraries

The idea that Rust should do better in low-level embedded/barebone domain
currently dominate by C and C++ is not new at all and there is lot of important
work in that area being done already. For example, xargo makes cross-compilation
with embedded targets easy and there are developers working on defining [unsafe
Rust memory model](https://github.com/nikomatsakis/rust-memory-model), which is
the right step towards "no excuse to use C for new projects".

However, there is one problem that doesn't seem to be mentioned often and I
don't even know if there is any existing plan towards addressing it in the long
run - building up `[no_std]` library ecosystem.

The problem with using regular libraries is that they tend to rely on platform
features that are taken as given in a regular system but may be non-existent of
heavily restricted on a different platform. Consider multi-threading or
exception handling as an example - though it may very well be possible that
dynamic memory allocation is not available at all. And I don't only talk about
some IoT style devices, something like WebAssembly target has a lot of similar
requirements (at least until it starts exposing browser GC).

At the same time there is huge amount of useful code that can be written without
any platform assumptions, most importantly algorithms and containers (with
user-provided allocator). Not having to reimplement everything just because your
target platform is weird would be a dream.

Rust already does one important step in this direction by implementing iterators
inside `core` namespace, not `std`. That is really great because iterators allow
implementing very high-level abstractions which are lazy and can delegate
platform-specific decisions to caller code. Sadly, most libraries out there use
them through `std::iter` alias and won't work in `[no_std]` configuration even
if conceptually they should.

Situation with public crates is not nice at all though as there is no way to
search for libraries that are compatible with `[no_std]` or fit some specific
performance criteria (for example, O(1) memory usage). Memory allocation issue
has been a particular problem in my searches because it seems too common for
developer to just add few `clone()` calls here and there to avoid fighting
borrow checker and if that `clone()` requires `malloc` internally, it may
immediately become a show-stopper.

### Different take on ergonomics

I must admit I was one of (probably very few) developers being a bit
dissapointed with how [Rust 2017 ergonomics
initiative](https://blog.rust-lang.org/2017/03/02/lang-ergonomics.html) was
delivered in practce. Don't get me wrong - every single change was making
language better and it was totally worth the effort. But it felt like initiative
was mostly focused on reducing amount of needless typing like better implicit
lifetime deduction. But at least for me main challenge with Rust ergonomics is
not about typing or understanding lifetime spec - it is about lack of
higher-level idioms that one can stick to with an expectation that it won't
result in trying to bend language backwards.

Great example of how small language extensions can make this better is addition
of `?` operator for error handling which I liked a lot. Reason why it is so good
isn't just because of boilerplate it hides, but because it immediately says to
someone new to the language - "Hey, here is how we do error handling in the
idiomatic Rust and looks how nice does it look! Your code will looks as nice if
you do the same!". That is kind of ergonomic improvements I am looking forward
to the most.

Another approach how this can be improved is by documenting those idioms,
something in the spirit of [these library API
guidelines](https://rust-lang-nursery.github.io/api-guidelines) but making
suggestions not only about API but also internal design and structure. Wide
range of possibilities combined with an unusual design requirements can feel
staggering for a newcomer and providing a simpler set of rules/suggestions can
help a lot.

How about making Rust the first language with a slogan "Zero-overhead made easy"?

### Summary

I don't realistically expect any of problems I have mentioned to be solved in
2018 - those rather broad and generic topics. What would be great though is to
end 2018 with a clear vision of how to move forward with those and some initial
starting steps. Just that would make me happy already.

And if any of more knowledgable Rust devs can forward me towards any of existing
projects/issues/RFCs in this domain, consider me a volunteer ;)
